package com.jquiz.form;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Form base class
 */
abstract class BaseForm {

    abstract public ArrayList<String> getFieldsList();
    abstract public void validate();
    
    protected HashMap<String, String> values = new HashMap<>();
    protected HashMap<String, String> errors = new HashMap<>();
    
    public void setValue(String key, String value) {
        values.put(key, value);
    }
    
    public String getValue(String key) {
        try {
            return values.get(key) != null ? values.get(key) : "";
        } catch (Exception e) {
            return "";
        }
    }

    public void setError(String key, String value) {
        errors.put(key, value);
    }

    public String getError(String key) {
        return errors.containsKey(key) ? errors.get(key) : "";
    }
    
    public boolean hasErrors() {
        return errors.size() > 0;
    }
    
    public boolean hasError(String key) {
        return errors.containsKey(key);
    }
    
}
