package com.jquiz.form;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import com.jquiz.model.Member;

/**
 * Login form
 */
public class LoginForm extends BaseForm {

    public ArrayList<String> getFieldsList() {
        ArrayList<String> fields = new ArrayList<>();
        fields.add("username");
        fields.add("password");
        return fields;
    }

    public static LoginForm emptyForm() {
        LoginForm form = new LoginForm();
        form.setValue("username", "");
        form.setValue("password", "");
        return form;
    }

    public static LoginForm fromRequest(HttpServletRequest req) {
        LoginForm form = new LoginForm();
        form.setValue("username", req.getParameter("username"));
        form.setValue("password", req.getParameter("password"));
        return form;
    }

    public static LoginForm fromObject(Object obj) throws Exception {
        if (!(obj instanceof Member)) {
            throw new Exception("Required instance of Member");
        }

        LoginForm form = new LoginForm();
        form.setValue("username", ((Member)obj).username);
        form.setValue("password", "");
        return form;
    }

    public void validate() {
        String username = values.get("username").trim();
        if (username.equals("")) {
            errors.put("username", "Required");
        } else if (username.length() > 30) {
            errors.put("username", "Length is too big");
        } else if (!username.matches("^[a-z0-9]+$")) {
            errors.put("username", "Incorrect format");
        }

        String password = values.get("username").trim();
        if (password.equals("")) {
            errors.put("password", "Required");
        } else if (password.length() > 30) {
            errors.put("password", "Length is too big");
        }
    }
    
}
