package com.jquiz.service;

import java.util.*;

/**
 * Tools for manage members sessions
 */
public class MemberSession extends TimerTask {

    // TODO: maybe store session data in database?
    
    public static final Integer lifetime = 120*60; // minutes
    public static final Integer activityGCTimeout = 5*60; // minutes

    protected static MemberSession instance;
    protected HashMap<String, Object> objects = new HashMap<>();
    protected HashMap<String, Date> objectsActivity = new HashMap<>();
    private Timer activityTimer = new Timer();

    protected MemberSession() {}
    
    public static synchronized MemberSession getInstance() {
        if (instance == null) {
            instance = new MemberSession();
            instance.objects = new HashMap<>();
            instance.activityTimer.schedule(instance, activityGCTimeout*1000, activityGCTimeout*1000);
        }
        return instance;
    }

    public Object getObject(String key) {
        if (objects.containsKey(key)) {
            return objects.get(key);
        } else {
            return null;
        }
    }
    
    public void putObject(String key, Object obj) {
        objects.put(key, obj);
        objectsActivity.put(key, new Date());
    }
    
    public void removeObject(String key) {
        if (objects.containsKey(key)) objects.remove(key);
        if (objectsActivity.containsKey(key)) objectsActivity.remove(key);
    }

    public void updateActivity(String key) {
        if (objects.containsKey(key) && objectsActivity.containsKey(key)) {
            objectsActivity.put(key, new Date());
        } else {
            try{
                objects.remove(key);
                objectsActivity.remove(key);
            } catch (Exception e) {}
        }
    }

    /**
     * Objects activity "Garbage collector"
     */
    public void run() {
        Iterator it = objectsActivity.entrySet().iterator();
        Date now = new Date();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Date activity = (Date) pair.getValue();
            if (now.getTime() - activity.getTime() > lifetime*1000) {
                try{
                    objects.remove(pair.getKey());
                } catch (Exception e) {}
                it.remove();
            }
        }
    }
    
}
